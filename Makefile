TAGNAME=macodigo/youtube-dl-macodigo

all: build

build:
	docker build -t ${TAGNAME} .

push:
	docker push ${TAGNAME}

shell:
	docker run -it --entrypoint /bin/sh ${TAGNAME}

example:
	docker run -v $(PWD):/downloads ${TAGNAME} --help
