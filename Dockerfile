FROM alpine
MAINTAINER UP Leong <pio.leong@gmail.com>

## youtube-dl ##
##
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
RUN apk add -q --progress --update --no-cache youtube-dl
RUN youtube-dl --version
##

WORKDIR /downloads

#
ENTRYPOINT ["/usr/bin/youtube-dl"]
