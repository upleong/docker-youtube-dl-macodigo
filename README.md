# Docker image macodigo/youtube-dl-macodigo

### Configuration
* Alpine
* youtube-dl latest

### Docker Hub
Avaialble at, https://hub.docker.com/r/macodigo/youtube-dl-macodigo/

### Running the command
```
    docker run -v $(pwd):/downloads macodigo/youtube-dl-macodigo 'https://vimeo.com/253989945'
```
